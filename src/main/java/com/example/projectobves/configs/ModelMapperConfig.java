package com.example.projectobves.configs;

import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
@AllArgsConstructor
public class ModelMapperConfig {

    private final ApplicationContext appContext;

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STRICT)
                .setFieldMatchingEnabled(true)
                .setSkipNullEnabled(true)
                .setFieldAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PRIVATE);
        return mapper;
    }

    public interface ModuleMapperConfig{
        void configure();
    }

    @PostConstruct
    private void configureAllModules(){
        var test = appContext.getBeansOfType(ModuleMapperConfig.class);
        appContext.getBeansOfType(ModuleMapperConfig.class).values().forEach(ModuleMapperConfig::configure);
    }

}
