package com.example.projectobves.configs;

import com.example.projectobves.modules.user.domain.Role;
import com.example.projectobves.modules.user.repositories.UserRepository;
import com.example.projectobves.modules.user.utilLayer.exceptions.UserNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Класс, наследуется от @see WebSecurityConfigurerAdapter
 * представляет собой настройку авторизации/аутентификации
 * на данным момент настроен на Basic Auth
 */

@Configuration
@AllArgsConstructor
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final MyUserDetailsService detailsService;

    //В данном методе переопределяется какой UserDetailService будет использоваться
    //и сообщается какой Encoder используется для хеширования паролей
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(detailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                    //список ссылок доступных без ау-ции
                    //примечание: под неверной аутентификацией всеравно будет разворачивать на 401
                    .antMatchers("/registration", "/registration/**" ,"/").permitAll()
                    //включение ау-ции TODO на пока отключено, потом - включить
                    .anyRequest().authenticated()
                .and()
                    //TODO разобраться что это такое, но без него не работает ^_^
                    .httpBasic()
                .and();
                    //отключение защиты от межсайтовой подделки запроса, так как у нас REST Api
        http.csrf().disable();
    }

    /**
     * Класс, имплементация @see UserDetailsService
     * содержит логику по превращению @see User, найденного по логину, в @see UserDetails
     * для последующей передачи механизму аутентификации/авторизации
     */

    @Service
    @AllArgsConstructor
    public static class MyUserDetailsService implements UserDetailsService {

        private final UserRepository userRepository;

        @Override

        public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

            //ищет юзера по логину и проверяет на нулл с выбросом ошибки
            var user = userRepository.findByLogin(username).orElseThrow(UserNotFoundException::new);

            return new org.springframework.security.core.userdetails.User(
                    user.getLogin(),
                    user.getPasswordHash(),
                    //превращение сета ролей юзера в массив названий ролей
                    AuthorityUtils.createAuthorityList(user.getRoles().stream().map(Role::getName).toArray(String[]::new)));
        }
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
