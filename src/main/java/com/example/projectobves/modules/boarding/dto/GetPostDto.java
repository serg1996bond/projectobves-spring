package com.example.projectobves.modules.boarding.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GetPostDto {
    private Integer boardId;
    private String name;
    private GetPostContentBlockDto[] contentBlockDtos;
}
