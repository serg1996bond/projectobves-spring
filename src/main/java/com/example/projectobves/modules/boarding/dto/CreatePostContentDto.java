package com.example.projectobves.modules.boarding.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreatePostContentDto {
    private String contentOfBlock;
    private Integer order;
}
