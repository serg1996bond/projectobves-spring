package com.example.projectobves.modules.boarding.controllers;

import com.example.projectobves.modules.boarding.domain.entities.Post;
import com.example.projectobves.modules.boarding.dto.CreateNewPostCommand;
import com.example.projectobves.modules.boarding.dto.GetPostDto;
import com.example.projectobves.modules.boarding.optionalLayer.PostsOptionalService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/post")
@AllArgsConstructor
public class PostController {

    private final PostsOptionalService postsOptionalService;

    @PostMapping("/createInBoard")
    private void CreateInBoard(@RequestBody CreateNewPostCommand command) {
        postsOptionalService.createNewPostInBoard(command);
    }

    @GetMapping("/allForBoard{boardId}")
    private GetPostDto[] allForBoardId(@PathVariable Integer boardId) {
        return postsOptionalService.getAllPostsByBoardId(boardId);
    }

}
