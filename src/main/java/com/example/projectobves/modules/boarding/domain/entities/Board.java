package com.example.projectobves.modules.boarding.domain.entities;

import com.example.projectobves.globalUtilLayer.entetiesUtil.entity.BaseEntety;
import com.example.projectobves.modules.user.domain.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Класс представляет собой обвертку коллекции объектов @see Post
 * также имеет параметр тематики <b>theme</b>
 * каждая Board неявно пренадлежит @see User
 * является СУЩНОСТЬЮ-АГРЕГАТОМ
 */

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Table(name = "boards")
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class Board extends BaseEntety {

    @Column(name = "board_name")
    /**имя доски*/
    private String name;

    /**тематика доски*/
    @Column(name = "board_theme")
    private String theme;

    /**список @see Post доски*/
    @OneToMany
    @JoinColumn(name = "board_id")
    private Set<Post> posts;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id")
    private User owner;

    /**добавляет @see Post в список постов*/
    public boolean addPost(Post post){
        return posts.add(post);
    }

    /**удаляет @see Post из списка постов*/
    public boolean remove(Post post){
        return posts.remove(post);
    }
}
