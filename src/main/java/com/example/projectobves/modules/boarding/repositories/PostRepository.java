package com.example.projectobves.modules.boarding.repositories;

import com.example.projectobves.modules.boarding.domain.entities.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface PostRepository extends CrudRepository<Post, Integer> {
    Set<Post> findAllByBoardId(Integer boardId);
}
