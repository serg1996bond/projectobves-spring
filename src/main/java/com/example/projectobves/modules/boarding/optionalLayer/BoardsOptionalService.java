package com.example.projectobves.modules.boarding.optionalLayer;

import com.example.projectobves.modules.boarding.domain.entities.Board;
import com.example.projectobves.modules.boarding.repositories.BoardRepository;
import com.example.projectobves.modules.boarding.dto.CreateBoardForCurrentUserCommand;
import com.example.projectobves.modules.boarding.dto.GetBoardDto;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@AllArgsConstructor
public class BoardsOptionalService {

    private final BoardRepository boardRepository;
    private final ModelMapper mapper;

    public void addBoardForCurrentUser(CreateBoardForCurrentUserCommand createBoardForCurrentUserCommand) {
        var createdBoard = this.mapper.map(createBoardForCurrentUserCommand,Board.class);
        boardRepository.save(createdBoard);
    }

    public Set<GetBoardDto> getAllBoardsByUserId(Integer userId){
        var boards = boardRepository.findAllByOwnerId(userId);
        return mapper.<HashSet<GetBoardDto>>map(boards, new TypeToken<HashSet<GetBoardDto>>(){}.getType());
    }
}
