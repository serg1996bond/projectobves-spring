package com.example.projectobves.modules.boarding.controllers;

import com.example.projectobves.modules.boarding.optionalLayer.BoardsOptionalService;
import com.example.projectobves.modules.boarding.dto.CreateBoardForCurrentUserCommand;
import com.example.projectobves.modules.boarding.dto.GetBoardDto;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * класс-контроллер для работы с @Board
 */

@RestController
@RequestMapping("/board")
@AllArgsConstructor
public class BoardController {

    //TODO придумать что-то со scope
    private final BoardsOptionalService optionalService;

    @PostMapping("/addForCurrentUser")
    public void addBoardForCurrentUser(@RequestBody CreateBoardForCurrentUserCommand createBoardForCurrentUserCommand) {
        optionalService.addBoardForCurrentUser(createBoardForCurrentUserCommand);
    }

    @GetMapping("/allByUID{userId}")
    public Set<GetBoardDto> getAllBoardsByOwnerId(@PathVariable Integer userId){
        return optionalService.getAllBoardsByUserId(userId);
    }

}
