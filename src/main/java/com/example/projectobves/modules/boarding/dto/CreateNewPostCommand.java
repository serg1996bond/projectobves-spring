package com.example.projectobves.modules.boarding.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CreateNewPostCommand {

    private String name;
    private CreatePostContentDto[] contentDtos;
    private Integer boardId;

}
