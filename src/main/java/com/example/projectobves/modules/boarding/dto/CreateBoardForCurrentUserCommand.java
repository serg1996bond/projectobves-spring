package com.example.projectobves.modules.boarding.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class CreateBoardForCurrentUserCommand {
    private final String name;
    private final String theme;
}
