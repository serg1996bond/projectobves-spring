package com.example.projectobves.modules.boarding.repositories;

import com.example.projectobves.modules.boarding.domain.entities.Board;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface BoardRepository extends CrudRepository<Board,Integer> {
    Set<Board> findAllByOwnerId(Integer ownerId);
}
