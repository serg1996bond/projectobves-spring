package com.example.projectobves.modules.boarding.utilLayer.mapperConfigs;

import com.example.projectobves.configs.ModelMapperConfig;
import com.example.projectobves.modules.boarding.domain.entities.Board;
import com.example.projectobves.modules.boarding.domain.entities.Post;
import com.example.projectobves.modules.boarding.domain.valueObjects.PostContentBlock;
import com.example.projectobves.modules.boarding.dto.*;
import com.example.projectobves.modules.boarding.repositories.BoardRepository;
import com.example.projectobves.modules.boarding.utilLayer.exceptions.BoardNotFoundException;
import com.example.projectobves.modules.user.domain.User;
import com.example.projectobves.modules.user.repositories.UserRepository;
import com.example.projectobves.modules.user.utilLayer.exceptions.UserNotFoundException;
import lombok.AllArgsConstructor;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Scope(value = "prototype")
@AllArgsConstructor
public class BoardingMapperConfig implements ModelMapperConfig.ModuleMapperConfig {

    private final CurrentUserConverter currentUserConverter;
    private final BoardIdToBoardConverter boardIdToBoardConverter;
    private final CreateNewPostCommandToPostConverter createNewPostCommandToPostConverter;
    //TODO придумать что-то с названиями конвертеров
    private final SetOfPostContentBlocksToArrayOfGetPostContentBlockDtoConverter setOfPostContentBlocksToArrayOfGetPostContentBlockDtoConverter;
    private final ModelMapper mapper;

    @Override
    public void configure(){
        this.mapper.typeMap(CreateBoardForCurrentUserCommand.class, Board.class)
                .addMapping(CreateBoardForCurrentUserCommand::getName,Board::setName)
                .addMapping(CreateBoardForCurrentUserCommand::getTheme,Board::setTheme)
                .addMapping(dto->new HashSet<Post>(), Board::setPosts)
                .addMappings(mapping -> mapping.using(currentUserConverter).map(CreateBoardForCurrentUserCommand::getName,Board::setOwner));
        this.mapper.typeMap(CreateNewPostCommand.class, Post.class)
                .addMapping(CreateNewPostCommand::getName, Post::setName)
                .addMappings(mapping -> mapping.using(createNewPostCommandToPostConverter)
                        .map(CreateNewPostCommand::getContentDtos,Post::setPostContentBlocks))
                .addMappings(mapping -> mapping.using(boardIdToBoardConverter)
                        .map(CreateNewPostCommand::getBoardId, Post::setBoard));
        this.mapper.typeMap(CreatePostContentDto.class, PostContentBlock.class)
                .addMapping(CreatePostContentDto::getContentOfBlock, PostContentBlock::setContentOfBlock)
                .addMapping(CreatePostContentDto::getOrder, PostContentBlock::setOrder);
        this.mapper.typeMap(Post.class, GetPostDto.class)
                .addMapping(Post::getName, GetPostDto::setName)
                .addMapping(post -> post.getBoard().getId(), GetPostDto::setBoardId)
                .addMappings(mapping -> mapping.using(setOfPostContentBlocksToArrayOfGetPostContentBlockDtoConverter)
                        .map(Post::getPostContentBlocks, GetPostDto::setContentBlockDtos));
    }

    @Component
    @Scope("prototype")
    @AllArgsConstructor
    private static class SetOfPostContentBlocksToArrayOfGetPostContentBlockDtoConverter
            extends AbstractConverter<Set<PostContentBlock>, GetPostContentBlockDto[]> {

        @Override
        protected GetPostContentBlockDto[] convert(Set<PostContentBlock> source) {
            return source.stream().map(postContentBlock ->
                    new GetPostContentBlockDto(postContentBlock.getContentOfBlock(),postContentBlock.getOrder()))
                    .toArray(GetPostContentBlockDto[]::new);
        }
    }

    @Component
    @Scope("prototype")
    @AllArgsConstructor
    private static class BoardIdToBoardConverter extends AbstractConverter<Integer, Board> {

        private final BoardRepository boardRepository;

        @Override
        protected Board convert(Integer source) {
            return boardRepository.findById(source).orElseThrow(BoardNotFoundException::new);
        }
    }

    @Component
    @Scope(value = "prototype")
    @AllArgsConstructor
    private static class CreateNewPostCommandToPostConverter
            extends AbstractConverter<CreatePostContentDto[], Set<PostContentBlock>> {

        @Override
        protected Set<PostContentBlock> convert(CreatePostContentDto[] source) {
            return Arrays.stream(source).map(createPostContentDto ->
                    new PostContentBlock(createPostContentDto.getContentOfBlock(),createPostContentDto.getOrder()))
                    .collect(Collectors.toCollection(HashSet<PostContentBlock>::new));
        }
    }

    @Component
    @Scope(value = "prototype")
    @AllArgsConstructor
    private static class CurrentUserConverter implements Converter<Object,User>{

        private final UserRepository userRepository;

        @Override
        public User convert(MappingContext<Object, User> context) {
            var userDetails = (org.springframework.security.core.userdetails.User) SecurityContextHolder
                    .getContext().getAuthentication().getPrincipal();
            var user = userRepository.findByLogin(userDetails.getUsername())
                    .orElseThrow(UserNotFoundException::new);
            return user;
        }
    }



}