package com.example.projectobves.modules.boarding.domain.valueObjects;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Table;

/**
 * класс является ОБЪЕКТОМ-ЗНАЧЕНИЕМ
 * Представляет собой блок содержимого поста
 */

//TODO пока зделаю просто с текстом, чтоб работало, потом зарефакторю и сделаю под одним интерфейсом
// наследники текстовый и картиночный и так далее блоки

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "post_content_blocks_table")
public class PostContentBlock {

    /**непосредственно содержимое блока содержимого XD*/
    @Column(name = "content_of_block")
    private String contentOfBlock;

    /**переменная нужна для упорядоченного вывода блоков и хранит номер в порядке*/
    @Column(name = "block_order")
    private Integer order;

}
