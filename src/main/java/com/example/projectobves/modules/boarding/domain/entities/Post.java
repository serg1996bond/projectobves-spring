package com.example.projectobves.modules.boarding.domain.entities;

import com.example.projectobves.globalUtilLayer.entetiesUtil.entity.BaseEntety;
import com.example.projectobves.modules.boarding.domain.valueObjects.PostContentBlock;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Table(name = "posts")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Post extends BaseEntety {

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "board_id")
    private Board board;

    @ElementCollection
    @CollectionTable(name = "post_content_blocks_table", joinColumns = @JoinColumn(name = "post_id"))
    private Set<PostContentBlock> postContentBlocks;
}
