package com.example.projectobves.modules.boarding.optionalLayer;

import com.example.projectobves.modules.boarding.domain.entities.Post;
import com.example.projectobves.modules.boarding.dto.CreateNewPostCommand;
import com.example.projectobves.modules.boarding.dto.GetPostDto;
import com.example.projectobves.modules.boarding.repositories.PostRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class PostsOptionalService {

    private final PostRepository postRepository;
    private final ModelMapper mapper;

    public void createNewPostInBoard(CreateNewPostCommand command) {
        var createdPost = mapper.map(command, Post.class);
        postRepository.save(createdPost);
    }

    public GetPostDto[] getAllPostsByBoardId(Integer boardId) {
        var posts = postRepository.findAllByBoardId(boardId);
        return posts.stream().map(post -> mapper.map(post, GetPostDto.class)).toArray(GetPostDto[]::new);
    }
}
