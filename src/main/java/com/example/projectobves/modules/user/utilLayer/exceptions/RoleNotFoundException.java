package com.example.projectobves.modules.user.utilLayer.exceptions;

import com.example.projectobves.globalUtilLayer.exceptions.ThisAppException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class RoleNotFoundException extends ThisAppException {
}
