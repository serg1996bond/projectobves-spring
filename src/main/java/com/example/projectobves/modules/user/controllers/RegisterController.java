package com.example.projectobves.modules.user.controllers;

import com.example.projectobves.modules.user.dto.RegisterUserCommand;
import com.example.projectobves.modules.user.optionalLayer.RegisterService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("registration")
@AllArgsConstructor
public class RegisterController {

    RegisterService registerService;

    @ResponseStatus(HttpStatus.OK)
    @PostMapping("new")
    public void registerNewUser(@RequestBody RegisterUserCommand registerUserCommand){
        registerService.RegisterUser(registerUserCommand);
    }

}
