package com.example.projectobves.modules.user.domain;

import com.example.projectobves.globalUtilLayer.entetiesUtil.entity.BaseEntety;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Класс представление - пользователя
 */

@Entity
@Table(name = "users")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User extends BaseEntety {

    @Column(name = "login")
    /**логин пользователя*/
    private String login;
    @Column(name = "password_hash")
    /**хеш пароля пользователя*/
    private String passwordHash;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_roles",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
    /**список ролей пользователя*/
    private Set<Role> roles;

}
