package com.example.projectobves.modules.user.utilLayer.mapperConfigs;

import com.example.projectobves.configs.ModelMapperConfig;
import com.example.projectobves.modules.user.domain.Role;
import com.example.projectobves.modules.user.domain.User;
import com.example.projectobves.modules.user.repositories.RolesRepository;
import com.example.projectobves.modules.user.repositories.UserRepository;
import com.example.projectobves.modules.user.dto.RegisterUserCommand;
import com.example.projectobves.modules.user.utilLayer.exceptions.LoginAreUsedException;
import com.example.projectobves.modules.user.utilLayer.exceptions.RoleNotFoundException;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Scope(value = "prototype")
@AllArgsConstructor
public class UserModuleMapper implements ModelMapperConfig.ModuleMapperConfig {

    private final ModelMapper mapper;
    private final PasswordEncoder encoder;
    private final RolesRepository rolesRepository;
    private final UserRepository userRepository;

    @Override
    public void configure() {
        //TODO тут был эксперимент с лямбдами... задуматься как делать лучше
        this.mapper.typeMap(RegisterUserCommand.class, User.class)
                .addMappings(mapping -> mapping.using(context -> {
                        //достает логин из ДТО
                        var login = (String) context.getSource();
                        //проверяет логин на занятость (если да - выбрасывает ошибку)
                        if (userRepository.existsByLogin(login)) throw new LoginAreUsedException();
                        //если нет - возвращает логин
                        return login;
                    }).map(RegisterUserCommand::getLogin,User::setLogin))
                //просто хеширует пороль и передает его юзеру
                .addMappings(mapping -> mapping.using(context -> encoder.encode((String) context.getSource()))
                        .map(RegisterUserCommand::getPassword,User::setPasswordHash))
                //вышло что-то некрасивое, может потом разберусь
                .addMappings(
                        mapping -> mapping
                                //лямбда-конвектор из дто берет сет имен ролей и запускает его стрим
                                .using(context -> ((Set<String>) context.getSource()).stream()
                                        //в стриме по каждому имени роли ищет роль в бд
                                        .map(roleName -> rolesRepository.findByName(roleName)
                                                //иначе выброс ошибки что в бд такого нет
                                                .orElseThrow(RoleNotFoundException::new))
                                        //результат соберается в хешсет ролей
                                        .collect(Collectors.toCollection(HashSet<Role>::new)))
                                .map(RegisterUserCommand::getRoles,User::setRoles));
    }
}
