package com.example.projectobves.modules.user.domain;

import com.example.projectobves.globalUtilLayer.entetiesUtil.entity.BaseEntety;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Класс-роль пользователя, используется для ауторизации
 */

@Entity
@Table(name = "roles")
@Getter
@Setter
public class Role extends BaseEntety {

    /**поле-название роли*/
    @Column(name = "name")
    private String name;

}
