package com.example.projectobves.modules.user.optionalLayer;

import com.example.projectobves.modules.user.domain.User;
import com.example.projectobves.modules.user.repositories.UserRepository;
import com.example.projectobves.modules.user.dto.RegisterUserCommand;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RegisterService {

    private final UserRepository userRepository;
    private final ModelMapper mapper;

    public void RegisterUser(RegisterUserCommand registerUserCommand) {
        var registeredUser = mapper.map(registerUserCommand,User.class);
        userRepository.save(registeredUser);
    }
}
