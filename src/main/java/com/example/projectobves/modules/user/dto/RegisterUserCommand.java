package com.example.projectobves.modules.user.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Set;

@AllArgsConstructor
@Getter
public class RegisterUserCommand {
    private final String login;
    private final String password;
    private final Set<String> roles;
}
