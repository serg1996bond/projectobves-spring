package com.example.projectobves.modules.user.repositories;

import com.example.projectobves.modules.user.domain.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface RolesRepository extends CrudRepository<Role, Integer> {
    Optional<Role> findByName(String name);
}
