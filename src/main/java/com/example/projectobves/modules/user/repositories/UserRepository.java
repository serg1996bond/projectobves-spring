package com.example.projectobves.modules.user.repositories;

import com.example.projectobves.modules.user.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    boolean existsByLogin(String login);
    Optional<User> findByLogin(String login);
}
