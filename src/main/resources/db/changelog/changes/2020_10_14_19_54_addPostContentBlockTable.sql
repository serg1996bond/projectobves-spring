create TABLE post_content_blocks_table
(
    post_id integer not null ,
    content_of_block text not null ,
    block_order integer not null
)