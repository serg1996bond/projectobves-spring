create TABLE roles
(
    id              serial      not null ,
    name            varchar     not null ,
    created_date    timestamp   not null,
    updated_date    timestamp   null,
    removed_date    timestamp   null,
    primary key     (id)
)