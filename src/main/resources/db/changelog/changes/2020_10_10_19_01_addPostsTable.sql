create TABLE posts
(
    id              serial      not null,
    name            varchar     not null ,
    board_id        int         not null,
    created_date    timestamp   not null,
    updated_date    timestamp   null,
    removed_date    timestamp   null,
    primary key     (id),
    foreign key     (board_id)  references boards (id)

)