create TABLE boards
(

    id              serial      not null,
    board_name      varchar     not null,
    board_theme     varchar     not null,
    created_date    timestamp   not null,
    updated_date    timestamp   null,
    removed_date    timestamp   null,
    primary key     (id)

)